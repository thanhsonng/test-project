// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import fs from 'fs';
import path from 'path';

type Data = {
  content: string
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const svg = fs.readFileSync(
    path.join(process.cwd(), "img", "file.svg"),
    "utf-8",
  );
  res.status(200).json({ content: svg })
}
